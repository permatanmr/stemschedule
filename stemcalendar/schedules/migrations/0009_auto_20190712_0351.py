# -*- coding: utf-8 -*-
# Generated by Django 1.11.13 on 2019-07-12 03:51
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('schedules', '0008_auto_20190712_0350'),
    ]

    operations = [
        migrations.AlterField(
            model_name='student',
            name='kelas',
            field=models.CharField(choices=[('A', 'A'), ('B', 'B'), ('C', 'C'), ('D', 'D'), ('E', 'E')], max_length=10),
        ),
    ]
