# -*- coding: utf-8 -*-
# Generated by Django 1.11.13 on 2019-08-08 09:01
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('schedules', '0009_auto_20190712_0351'),
    ]

    operations = [
        migrations.CreateModel(
            name='ClassSlot',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('hari', models.CharField(choices=[('Senin', 'Senin'), ('Selasa', 'Selasa'), ('Rabu', 'Rabu'), ('Kamis', 'Kamis'), ('Jumat', 'Jumat')], max_length=10)),
                ('ruang', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='ruang', to='schedules.Room')),
            ],
        ),
        migrations.CreateModel(
            name='MataKuliah',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_on', models.DateTimeField(auto_now_add=True)),
                ('subject_code', models.CharField(blank=True, max_length=12, null=True)),
                ('subject_name', models.CharField(max_length=30, unique=True)),
                ('max_credit', models.IntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='TimeSlot',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_on', models.DateTimeField(auto_now_add=True)),
                ('time_at', models.CharField(max_length=40)),
            ],
        ),
        migrations.RemoveField(
            model_name='schedule',
            name='dosen',
        ),
        migrations.RemoveField(
            model_name='schedule',
            name='mahasiswa',
        ),
        migrations.RemoveField(
            model_name='schedule',
            name='ruang',
        ),
        migrations.AddField(
            model_name='lecturer',
            name='created_on',
            field=models.DateTimeField(auto_now_add=True, default=django.utils.timezone.now),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='lecturer',
            name='status',
            field=models.CharField(choices=[('Fulltime', 'Fulltime'), ('Partime', 'Partime')], default=django.utils.timezone.now, max_length=10),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='lecturer',
            name='nama',
            field=models.CharField(max_length=40),
        ),
        migrations.DeleteModel(
            name='Schedule',
        ),
        migrations.AddField(
            model_name='classslot',
            name='waktu_mulai',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='waktu_mulai', to='schedules.TimeSlot'),
        ),
        migrations.AddField(
            model_name='classslot',
            name='waktu_selesai',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='waktu_selesai', to='schedules.TimeSlot'),
        ),
    ]
