# -*- coding: utf-8 -*-
# Generated by Django 1.11.13 on 2019-02-11 10:03
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('schedules', '0003_room_day'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='room',
            name='day',
        ),
        migrations.AddField(
            model_name='schedule',
            name='day',
            field=models.DateField(default=None, help_text='Day of the event', verbose_name='Day of the event'),
            preserve_default=False,
        ),
    ]
