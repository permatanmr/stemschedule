# Generated by Django 2.2.6 on 2019-11-07 07:20

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('schedules', '0026_auto_20190828_0828'),
    ]

    operations = [
        migrations.CreateModel(
            name='Kuliah',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_on', models.DateTimeField(auto_now_add=True)),
                ('class_date', models.DateTimeField(auto_now_add=True)),
                ('day', models.DateTimeField(auto_now_add=True)),
                ('start_time', models.DateTimeField(auto_now_add=True)),
                ('end_time', models.DateTimeField(auto_now_add=True)),
                ('full_name', models.CharField(blank=True, max_length=100, null=True)),
                ('initial', models.CharField(blank=True, max_length=10, null=True)),
                ('subject_code', models.CharField(blank=True, max_length=20, null=True)),
                ('subject_name', models.CharField(blank=True, max_length=100, null=True)),
                ('cohort_name', models.CharField(blank=True, max_length=100, null=True)),
                ('room_no', models.CharField(blank=True, max_length=100, null=True)),
            ],
        ),
    ]
