# -*- coding: utf-8 -*-
# Generated by Django 1.11.13 on 2019-07-12 03:50
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('schedules', '0007_auto_20190712_0343'),
    ]

    operations = [
        migrations.AddField(
            model_name='student',
            name='angkatan',
            field=models.CharField(choices=[('2018', '2018'), ('2019', '2019'), ('2020', '2020'), ('2021', '2021')], default='', max_length=10),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='student',
            name='kelas',
            field=models.CharField(choices=[('A', 'A'), ('B', 'B'), ('C', 'C')], max_length=10),
        ),
    ]
